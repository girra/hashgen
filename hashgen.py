import sys
import sqlite3
from hashlib import md5
from hashlib import sha1

password_list = sys.argv[1]
ITOA64 = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

def wordpress_passwd(password, salt, count, prefix, uppercase=False):

    def _encode64(input_, count):
        output = ''
        i = 0
        while i < count:
            value = ord(input_[i])
            i += 1
            output = output + ITOA64[value & 0x3f]
            if i < count:
                value = value | (ord(input_[i]) << 8)
            output = output + ITOA64[(value >> 6) & 0x3f]
            i += 1
            if i >= count:
                break
            if i < count:
                value = value | (ord(input_[i]) << 16)
            output = output + ITOA64[(value >> 12) & 0x3f]
            i += 1
            if i >= count:
                break
            output = output + ITOA64[(value >> 18) & 0x3f]
        return output
    cipher = md5(salt)
    cipher.update(password)
    hash_ = cipher.digest()
    for i in xrange(count):
        _ = md5(hash_)
        _.update(password)
        hash_ = _.digest()
    retVal = prefix + _encode64(hash_, 16)
    return retVal.upper() if uppercase else retVal


def mysql_passwd(password, uppercase=True):
    retVal = "*%s" % sha1(sha1(password).digest()).hexdigest()
    return retVal.upper() if uppercase else retVal.lower()

def main():
    conn = sqlite3.connect("hash.db")
    conn.text_factory = str
    c = conn.cursor()

    if sys.argv[1] == "wp":
        password_list = sys.argv[2]
        salt = sys.argv[3]
        c.execute("CREATE TABLE IF NOT EXISTS wp (id INT PRIMARY KEY, hash TEXT, password TEXT, salt TEXT)")
        with open(password_list, 'r') as passlist:
            for password in passlist:
                password = password.replace('\n', '')
                wp_h = wordpress_passwd(password=password, salt=salt, count=2048, prefix='$P$B%s' %(salt), uppercase=False)
                c.execute("INSERT INTO wp (hash, password, salt) VALUES (?,?,?)", (wp_h, password, salt))

    elif sys.argv[1] == "mysql":
        c.execute("CREATE TABLE IF NOT EXISTS mysql (id INT PRIMARY KEY, password TEXT, hash TEXT)")
        password_list = sys.argv[2]
        with open(password_list, 'r') as passlist:
            for password in passlist:
                password = password.replace('\n', '')
                my_h = mysql_passwd(password, uppercase=True)
                c.execute("INSERT INTO mysql (password, hash) VALUES (?,?)", (password, my_h))

    conn.commit()
    c.close()

main()
