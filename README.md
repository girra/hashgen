# README #

Это генератор MySQL- и WordPress-хэшей.


```

$> python2 hashgen.py mysql passwordlist.txt
```


`mysql` - это тип хэша, а `passwordlist.txt` - список паролей соответственно.

```
$> python2 hashgen.py wp passwordlist.txt wt3YA7z2
```

`wp` - тип хэша, `passwordlist.txt` - список паролей, `wt3YA7z2` - соль.

определение соли  $P$B`wt3YA7z2`hkhwzhUqwT4EBLOag/2XZ.

![Снимок экрана_2015-09-28_18-26-06.png](https://bitbucket.org/repo/MMXAbk/images/2737332264-%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0_2015-09-28_18-26-06.png)